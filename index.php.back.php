<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
	
	<?php // check if is dev :
	function is_dev() {
		$dev = array('local\.','dev\.','192\.168\.','localhost');
		$reg = implode('|', $dev);
		if (	( preg_match('#'.$reg.'#isU', $_SERVER['HTTP_HOST'])
				|| ! is_file('js/app.js') )
			&& $_SERVER['SERVER_PORT'] != 8888) return true;
		else return false;
	} ?>

	<!-- metas -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width">
	<!-- end metas -->

	<!-- opengraph -->
	<meta property="og:title" content="..." />
	<meta property="og:description" content="..."/>
	<meta property="og:url" content="<?= 'http://'. $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ?>"/>
	<meta property="og:site_name" content="..."/>
	<meta property="og:image" content="..."/>
	<!-- end opengraph -->

	<!-- title -->
	<title>...</title>

   	<!-- css -->
	<link rel="stylesheet" href="css/style.css">
	
	<?php // head scripts :
	if (is_dev()): ?>
		<!-- build:js js/build/app.head.js -->
		<script src="js/vendor/modernizr.js"></script>
		<script src="js/vendor/respond.js"></script>
		<script src="js/vendor/cssua.js"></script>
		<!-- endbuild -->
	<?php else: ?>
		<script src="js/build/app.head.js"></script>
	<?php endif; ?>

</head>
<body>

	<h1>
		Hello World
	</h1>

	<?php // scripts management :
	if (is_dev()): ?>
		<!-- build:js js/build/app.js -->
		<script src="js/main.js"></script>
		<script src="js/GA.js"></script>
		<!-- endbuild -->
	<?php else: ?>
		<!-- js -->
		<script src="js/build/app.js"></script>
	<?php endif; ?>

</body>
</html>
