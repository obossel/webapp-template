// Generated on 2013-12-04 using generator-webapp 0.4.4
'use strict';

// # Globbing
// for performance reasons we're only matching one level down:
// 'test/spec/{,*/}*.js'
// use this if you want to recursively match all subfolders:
// 'test/spec/**/*.js'

module.exports = function (grunt) {
	
	// show elapsed time at the end
	require('time-grunt')(grunt);
	// load all grunt tasks
	require('load-grunt-tasks')(grunt);

	// configuration :
	grunt.initConfig({
		
		/**
		 * PHP Server :
		 */
		php: {
			dev : {
				options : {
					port: 6969,
					base: '.',
					open: true,
					hostname : 'localhost',
					keepalive: true
				}
			},
			prod : {
				options : {
					port: 6868,
					base: '.',
					open: true,
					hostname : 'localhost',
					keepalive: true
				}
			}
		},

		/**
		 * Watch files :
		 */
		watch: {
			options: {
				livereload: true,
			},
			src : {
				files : [
					'{,*/}*.{php,html,jpg,jpeg,gif,bmp,png}'
				],
				tasks : []
			},
			js: {
				files: ['js/{,*/}*.js'],
				tasks: []
			},
			gruntfile: {
				files: ['Gruntfile.js']
			},
			compass: {
				files: ['sass/{,*/}*.{scss,sass}'],
				tasks: ['compass:compile','notify:sass']
			},
			styles: {
				files: ['css/{,*/}*.css'],
				tasks: []
			}
		},

		/**
		 * Compile sass :
		 */
		compass: {
			options: {
				config: 'config.rb'
			},
			compile : {
				options: {
                    debugInfo: true
                }
			}
		},

		/**
		 * Parse files to find js and css files to concat :
		 */
		useminPrepare: {
			options: {
				dest: './'
			},
			html: 'index.{html,php}'
		},

		/**
		 * Optimize images :
		 */
		imagemin: {
			optimize : {
				files : [{
					expand : true,
					cwd : 'img',
					src : '{,*/}*.{gif,jpeg,jpg,png}',
					dest : 'img'
				}]
			}
		},

		/**
		 * Parallels tasks bundle :
		 */
		concurrent: {
			devServe : {
				options : {
                    logConcurrentOutput : true
                },
				tasks : [
					'compass:compile',
					'watch',
					'php:dev'
				]
			},
			prodServe : {
				options : {
                    logConcurrentOutput : true
                },
				tasks : [
					'compass:compile',
					'watch',
					'php:prod'
				]
			},
			optimizeImages : [
				'imagemin:optimize'
			]
		},

		/**
		 * Send notifications :
		 */
		notify : {
			sass : {
				options : {
					title : 'SASS Sucess',  // optional
					message : 'SASS compile process success...', //required
				}
			},
			buildSuccess : {
				options : {
					title : 'Build Success',
					message : 'Build process success'
				}
			}
		}
	});

	// load tasks :
	grunt.loadNpmTasks('grunt-php');
	grunt.loadNpmTasks('grunt-contrib-imagemin');
	grunt.loadNpmTasks('grunt-notify');

	// server :
	grunt.registerTask('serve', function (target) {

		// prod server :
		if (target === 'prod') {
			return grunt.task.run([
				'build',
				'concurrent:prodServe',
				'notify'
			]);
		}

		// dev server :
		grunt.task.run([
			'build',
			'concurrent:devServe',
			'notify'
		]);
	});

	// normal build process :
	grunt.registerTask('build', [
		'useminPrepare',
		'concat',
		'uglify',
		'notify:buildSuccess'
	]);

	// full build process (with image optimizazion) :
	grunt.registerTask('fullBuild', [
		'useminPrepare',
		'concat',
		'uglify',
		'optimizeImages',
		'notify:buildSuccess'
	]);

	// optimize images :
	grunt.registerTask('optimizeImages', [
		'concurrent:optimizeImages'
	]);

	// default process => build :
	grunt.registerTask('default', [
		'build'
	]);
};
